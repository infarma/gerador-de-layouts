# Gerador de Layouts - V 0.0.3

#### Razão

Facilitar a geração de layouts através de uma ferramenta de geração de código.

#### Funcionamento

A ferramenta gera código por meio da passagem de argumentos via linha de comando. Tais argumentos devem ser separados por dois pontos e espaços.

A passagem dos argumentos segue a seguinte ordem:

1 - Nome do Pacote

2 - Nome da Struct

3 - Nome do atributo:Tipo do Atributo:Início no layout:Fim no layout

Obs: O terceiro passo pode ser repetido pela quantidade de atributos que o layout possui, separando sempre por espaço.

Ex.: 

```shell
nomeDoPacote nomeDaStruct atributo0:int32:0:5 atributo1:float64:5:7:2 atributo2:string:7:15
```

Obs: Note que para o atributo1 existe um valor extra passado após o valor de posição final do layout. Tal valor é referente a quantidade de casas decimais para um dado atributo float. Ele é utilizado no momento da tradução de string para o formato float/float32/float64.

O código acima geraria a seguinte struct:

````go
package nomeDoPacote

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type NomeDaStruct struct {
	atributo0 int32  	`json:"atributo0"`
	atributo1 float64	`json:"atributo1"`
	atributo2 string 	`json:"atributo2"`
}

func (n *NomeDaStruct) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesNomeDaStruct

	err = posicaoParaValor.ReturnByType(&n.atributo0, "atributo0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.atributo1, "atributo1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.atributo2, "atributo2")
	if err != nil {
		return err
	}

	return err
}

var PosicoesNomeDaStruct = map[string]gerador_layouts_posicoes.Posicao{
	"atributo0":                      {0, 5, 0},
	"atributo1":                      {5, 7, 2},
	"atributo2":                      {7, 15, 0},
}
````

#### Exceções

Após a passagem dos argumentos, uma verificação da validez dos parâmetros é feita. Quando a sintaxe está errada, na maior parte dos casos a ferramenta exibirá na tela o trecho com a sintaxe problemática.

Ex.:

Para a seguinte chamada (que é INVÁLIDA por não conter a posição final de layout para o atributo2)
```shell
nomeDoPacote nomeDaStruct atributo0:int32:0:5 atributo1:float64:5:7:2 atributo2:string:7
```

Seria exibida a seguinte mensagem:

```shell
Error while creating layout.  Wrong syntax: atributo2:string:7
```

#### TODO

* Escrever testes unitários
* Melhorar feedback
