package assets

import (
	"fmt"
	"strconv"
	"strings"
)

type Attribute struct {
	AttributeName string
	AttributeType string
	Start         int
	End           int
	DecimalPlaces int
}

func (a *Attribute) composeAttributes(argument string) {
	attrAndType := strings.Split(argument, ":")
	a.AttributeName, a.AttributeType = attrAndType[0], attrAndType[1]

	//Clean up later
	start, _ := strconv.ParseInt(attrAndType[2], 10, 32)

	a.Start = int(start)

	end, _ := strconv.ParseInt(attrAndType[3], 10, 32)

	switch a.AttributeType {
	case "float", "float32", "float64":
		decimalPlaces, _ := strconv.ParseInt(attrAndType[4], 10, 32)
		a.DecimalPlaces = int(decimalPlaces)
	default:
		a.DecimalPlaces = 0
	}

	a.End = int(end)
}

func (a *Attribute) composeAttributesForPackageClass(argument string) {
	a.AttributeName, a.AttributeType = argument, argument
}

func (a *Attribute) generateThisLineForStruct(sizeAttributeName, sizeAttributeType int) string {
	var sb strings.Builder

	//Adding initial indentation
	sb.WriteString("\t")

	//Writing attribute name
	sb.WriteString(fmt.Sprintf("%s", a.AttributeName))
	sb.WriteString(" ")
	//Writing attribute indentation based on the biggest attribute name
	sb.WriteString(fmt.Sprintf("%s", a.generateIdentationName(sizeAttributeName)))

	//Writing attribute type
	sb.WriteString(fmt.Sprintf("%s", a.AttributeType))
	//Writing attribute type indentation based on the biggest attribute name
	sb.WriteString(fmt.Sprintf("%s", a.generateIdentationType(sizeAttributeType)))

	//Writing JSON stuff
	sb.WriteString(fmt.Sprintf("\t`json:\"%s\"`\n", a.AttributeName))

	return sb.String()
}

func (a *Attribute) generateThisLineForPosition(sizeAttributeName, sizeAttributeType int) string {
	return fmt.Sprintf("\t\"%s\":                      {%d, %d, %d},\n", a.AttributeName, a.Start, a.End, a.DecimalPlaces)
}

func (a *Attribute) generateThisLineForStub(structName string, sizeAttributeName, sizeAttributeType int) string {
	var content strings.Builder
	content.WriteString(fmt.Sprintf("\terr = posicaoParaValor.ReturnByType(&%c.%s, \"%s\")\n", strings.ToLower(structName)[0], a.AttributeName, a.AttributeName))
	content.WriteString("\tif err != nil {\n")
	content.WriteString("\t\treturn err\n")
	content.WriteString("\t}\n\n")

	return content.String()
}

func (a *Attribute) generateIdentationName(sizeAttributeName int) string {
	return a.calculateIndentation(sizeAttributeName, len(a.AttributeName))
}

func (a *Attribute) generateIdentationType(sizeAttributeType int) string {
	return a.calculateIndentation(sizeAttributeType, len(a.AttributeType))
}

func (a *Attribute) calculateIndentation(longerAttribute, shorterAttribute int) string {
	var sb strings.Builder

	diff := longerAttribute - shorterAttribute

	for i := 1; i <= diff; i++ {
		sb.WriteString(" ")
	}

	return sb.String()
}
