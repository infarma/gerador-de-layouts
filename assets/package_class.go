package assets

import (
	"bufio"
	"fmt"
	"github.com/iancoleman/strcase"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type PackageClass struct {
	PackageFile    *os.File
	PackageName    string
	Attributes     []Attribute
	PackageContent strings.Builder
	SizeAttribute  int
}

func (p *PackageClass) GeneratePackageClass(packageName string) error {
	p.PackageName = packageName

	err := p.getAttributesFromFiles()
	if err != nil {
		return err
	}

	err = p.composePackageContent()
	if err != nil {
		return err
	}

	err = p.createAndWriteFile()
	if err != nil {
		return err
	}

	return nil
}

func (p *PackageClass) createFile() error {
	f, err := os.Create(fmt.Sprintf("%s//%s.go", p.PackageName, p.PackageName))
	if err != nil {
		return err
	}

	p.PackageFile = f
	return nil
}

func (p *PackageClass) getAttributesFromFiles() error {
	/* Search the /packageName folder for .go files */

	dirName := fmt.Sprintf("./%s/", p.PackageName)

	files, err := ioutil.ReadDir(dirName)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		x := len(f.Name())

		checkName := fmt.Sprintf("%s.go", strcase.ToLowerCamel(p.PackageName))

		/* If this is a .go file and its name isn't the same name held in p.PackageName */
		if f.Name()[x-2:] == "go" && f.Name() != checkName {
			/* Read files searching for the struct name*/
			err = p.searchForStructTypeInFile(fmt.Sprintf("%s%s", dirName, f.Name()))
			if err != nil {
				return err
			}
		}

	}

	return nil
}

func (p *PackageClass) searchForStructTypeInFile(filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "type") && strings.Contains(scanner.Text(), "struct {") {
			attributeType := scanner.Text()
			attributeType = strings.Replace(attributeType, "type ", "", 1)
			attributeType = strings.Replace(attributeType, " struct {", "", 1)
			attributeType = strings.TrimSpace(attributeType)

			var attr Attribute
			attr.composeAttributesForPackageClass(attributeType)

			p.checkLongestAttribute(attributeType)

			p.Attributes = append(p.Attributes, attr)
		}

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return nil
}

func (p *PackageClass) composePackageContent() error {
	/* Write package name at the top of the file */
	p.writePackageName()

	/* Write the struct type alongisde the attributes */
	p.writeStruct()

	/* Write function that calls every attribute's composeStruct function */
	p.writeFunc()

	return nil
}

func (p *PackageClass) writePackageName() {
	/* Write package */
	p.PackageContent.WriteString(fmt.Sprintf("package %s\n\n", strcase.ToLowerCamel(p.PackageName)))
}

func (p *PackageClass) writeStruct() {
	/* Write struct containing attributes from every file within this package */
	p.PackageContent.WriteString(fmt.Sprintf("type %s struct {\n", strcase.ToCamel(p.PackageName)))

	p.writeAttributes()
}

func (p *PackageClass) writeAttributes() {
	//Writing attributes/stubs/globalvar
	for i, attribute := range p.Attributes {
		p.PackageContent.WriteString(attribute.generateThisLineForStruct(p.SizeAttribute, p.SizeAttribute))
		if i == len(p.Attributes)-1 {
			p.PackageContent.WriteString("}\n\n")
		}
	}
}

func (p *PackageClass) checkLongestAttribute(attributeName string) {
	if len(attributeName) > p.SizeAttribute {
		p.SizeAttribute = len(attributeName)
	}
}

func (p *PackageClass) writeFunc() {
	/* Write struct containing attributes from every file within this package */

	p.PackageContent.WriteString(fmt.Sprintf("func (%c *%s) ComposeStruct(fileContents string) error {\n", strings.ToLower(p.PackageName)[0], strcase.ToCamel(p.PackageName)))
	p.PackageContent.WriteString("\tvar err error\n\n")

	for _, attribute := range p.Attributes {
		p.PackageContent.WriteString(fmt.Sprintf("\terr = %c.%s.ComposeStruct(fileContents)\n", strings.ToLower(p.PackageName)[0], attribute.AttributeName))
		p.PackageContent.WriteString("\tif err != nil {\n")
		p.PackageContent.WriteString("\t\t return err\n")
		p.PackageContent.WriteString("\t}\n\n")
	}

	p.PackageContent.WriteString("\treturn nil\n")
	p.PackageContent.WriteString("}\n\n")
}

func (p *PackageClass) createAndWriteFile() error {
	err := p.createFile()
	if err != nil {
		return err
	}

	_, err = p.PackageFile.WriteString(p.PackageContent.String())
	if err != nil {
		return err
	}

	return nil
}