package assets

import (
	"fmt"
	"github.com/iancoleman/strcase"
	"strings"
)

type Parameter struct {
	PackageName       string
	StructName        string
	SubPackageName    string
	PositionName      string
	Attributes        []Attribute
	SizeAttributeName int
	SizeAttributeType int
	StructContent     strings.Builder
	StructFuncContent strings.Builder
	PositionContent   strings.Builder
}

func (p *Parameter) GenerateLayout() {
	/* Write struct package name */
	p.StructContent.WriteString(p.generatePackageStruct())

	/* Write struct imports */
	p.StructContent.WriteString(p.generateStructImport())

	/* Write struct signature */
	p.StructContent.WriteString(p.generateStructSignature())

	/* Write function that composes the struct */
	p.StructFuncContent.WriteString(p.generateBaseFunc())

	/* Write position signature */
	p.PositionContent.WriteString(p.generatePositionSignature())

	/* Write attributes, stubs for function ComposeStruct and globalVar that contains the start,
	   end and decimal places for every attribute */
	for _, attribute := range p.Attributes {
		p.StructContent.WriteString(attribute.generateThisLineForStruct(p.SizeAttributeName, p.SizeAttributeType))
		p.StructFuncContent.WriteString(attribute.generateThisLineForStub(p.StructName, p.SizeAttributeName, p.SizeAttributeType))
		p.PositionContent.WriteString(attribute.generateThisLineForPosition(p.SizeAttributeName, p.SizeAttributeType))
	}

	p.StructContent.WriteString("}\n\n")

	p.StructFuncContent.WriteString("\treturn err\n}")

	p.StructContent.WriteString(p.StructFuncContent.String())

	p.PositionContent.WriteString("}")

	/* Append positions hashmap to structFunc */
	p.StructContent.WriteString("\n\n")
	p.StructContent.WriteString(p.PositionContent.String())
}

func (p *Parameter) ComposeParameters(arguments []string) {
	p.SizeAttributeName = 0
	p.PackageName = arguments[1]
	p.StructName = strcase.ToCamel(arguments[2])
	p.SubPackageName = fmt.Sprint("posicoes", strcase.ToCamel(p.PackageName))
	p.PositionName = fmt.Sprint("posicoes", strcase.ToCamel(p.StructName))

	p.ComposeAttributes(arguments[3:])
}

func (p *Parameter) ComposeAttributes(arguments []string) {
	for _, arg := range arguments {
		var attribute Attribute
		attribute.composeAttributes(arg)
		p.checkLongestAttribute(arg)
		p.Attributes = append(p.Attributes, attribute)
	}
}

func (p *Parameter) checkLongestAttribute(attribute string) {
	attrAndType := strings.Split(attribute, ":")
	p.checkLongestAttributeName(attrAndType[0])
	p.checkLongestAttributeType(attrAndType[1])
}

func (p *Parameter) checkLongestAttributeName(attributeName string) {
	if len(attributeName) > p.SizeAttributeName {
		p.SizeAttributeName = len(attributeName)
	}
}

func (p *Parameter) checkLongestAttributeType(attributeType string) {
	if len(attributeType) > p.SizeAttributeType {
		p.SizeAttributeType = len(attributeType)
	}
}

func (p *Parameter) generateStructContent() string {
	var sb strings.Builder

	//Writing package name
	sb.WriteString(p.generatePackageStruct())

	//Writing struct signature
	sb.WriteString(p.generateStructSignature())

	//Writing attributes
	for _, attribute := range p.Attributes {
		sb.WriteString(attribute.generateThisLineForStruct(p.SizeAttributeName, p.SizeAttributeType))
	}
	sb.WriteString("}")

	return sb.String()
}

func (p *Parameter) generatePosition() string {
	var sb strings.Builder

	//Writing package name
	sb.WriteString(p.generatePackagePosition())

	//Writing struct signature
	sb.WriteString(p.generatePositionSignature())

	////Writing attributes
	for _, attribute := range p.Attributes {
		sb.WriteString(attribute.generateThisLineForPosition(p.SizeAttributeName, p.SizeAttributeType))
	}
	sb.WriteString("}")

	return sb.String()
}

func (p *Parameter) generateStructFunc() string {
	var sb strings.Builder

	sb.WriteString(p.generateBaseFunc())

	////Writing attributes
	for _, attribute := range p.Attributes {
		sb.WriteString(attribute.generateThisLineForStub(p.StructName, p.SizeAttributeName, p.SizeAttributeType))
	}
	sb.WriteString("\n\treturn err\n}")

	return sb.String()
}

func (p *Parameter) generateBaseFunc() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("func (%c *%s) ComposeStruct(fileContents string) error {\n", strings.ToLower(p.StructName)[0], p.StructName))
	sb.WriteString("\tvar err error\n\n")
	sb.WriteString("\tvar posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor\n\n")
	sb.WriteString("\t//Passo o conteúdo do arquivo\n")
	sb.WriteString("\tposicaoParaValor.FileContents = fileContents\n\n")
	sb.WriteString("\t//Passo as posicoes referentes a esse struct\n")
	sb.WriteString(fmt.Sprintf("\tposicaoParaValor.Posicoes = %s\n\n", strcase.ToCamel(p.PositionName)))

	return sb.String()
}

func (p *Parameter) generatePackageStruct() string {
	return p.generatePackage(p.PackageName)
}

func (p *Parameter) generatePackagePosition() string {
	return p.generatePackage(p.SubPackageName)
}

func (p *Parameter) generatePackage(packageName string) string {
	return fmt.Sprintf("package %s\n\n", packageName)
}

func (p *Parameter) generateStructSignature() string {
	return fmt.Sprintf("type %s struct {\n", p.StructName)
}

func (p *Parameter) generateStructImport() string {
	return "import \"bitbucket.org/infarma/gerador-layouts-posicoes\"\n\n"
}

func (p *Parameter) generatePositionSignature() string {
	return fmt.Sprintf("var %s = map[string]gerador_layouts_posicoes.Posicao{\n", strcase.ToCamel(p.PositionName))
}

func (p *Parameter) returnPackageFolder() string {
	return p.PackageName
}

func (p *Parameter) returnSubPackageFolder() string {
	subPackage := fmt.Sprint(p.returnPackageFolder(), "//posicoes", strcase.ToCamel(p.PackageName))
	return subPackage
}

func (p *Parameter) returnPositionsFileName() string {
	subPackage := fmt.Sprint("//posicoes", strcase.ToCamel(p.StructName))
	return subPackage
}

func (p *Parameter) generateStruct() string {
	var sb strings.Builder
	sb.WriteString(p.generateStructContent())
	sb.WriteString("\n\n")
	sb.WriteString(p.generateStructFunc())
	return sb.String()
}
