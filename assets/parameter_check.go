package assets

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type ParameterCheck struct {
}

func (p *ParameterCheck) CheckParams(arguments []string) error {

	//Check if argument list meets the minimum requirement
	err := p.CheckArguments(arguments)
	if err != nil {
		return err
	}

	err = p.CheckAttributes(arguments[3:])
	if err != nil {
		return err
	}

	return nil
}

func (p *ParameterCheck) CheckArguments(arguments []string) error {
	if len(arguments) <= 3 {
		return errors.New("Not enough arguments.")
	}
	return nil
}

func (p *ParameterCheck) CheckAttributes(arguments []string) error {
	for _, arg := range arguments {
		err := p.CheckAttributeNamesTypesAndSizes(arg)
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *ParameterCheck) CheckAttributeNamesTypesAndSizes(argument string) error {
	attrAndType := strings.Split(argument, ":")

	/* Check if attribute parameter is correct */
	if len(attrAndType) < 4 {
		return errors.New(fmt.Sprintf("Wrong syntax: %s", argument))
	}

	/* Check if attribute is of type float, because then it should have a fifth value */
	err := p.CheckIfSpecialCases(attrAndType)
	if err != nil {
		return err
	}

	/* Check if start position is bigger than end position */
	err = p.CheckIfStartIsBiggerThanEnd(attrAndType)
	if err != nil {
		return err
	}

	/* Check if all arguments needed to compose an attribute are present */
	err = p.CheckComponents(argument, attrAndType)
	if err != nil {
		return err
	}

	return nil
}

func (p *ParameterCheck) CheckIfStartIsBiggerThanEnd(attrAndType []string) error {

	start, err := strconv.Atoi(attrAndType[2])
	if err != nil {
		return errors.New(fmt.Sprintf("Wrong syntax: %s\n'%s' is not a valid integer. You need to pass a valid integer to represent the start position. ", strings.Join(attrAndType[:], ":"), attrAndType[2]))
	}

	end, err := strconv.Atoi(attrAndType[3])
	if err != nil {
		return errors.New(fmt.Sprintf("Wrong syntax: %s\n'%s' is not a valid integer. You need to pass a valid integer to represent the end position. ", strings.Join(attrAndType[:], ":"), attrAndType[3]))
	}

	if start > end {
		return errors.New(fmt.Sprintf("Wrong syntax: %s.\nEnd position should be bigger than start position", strings.Join(attrAndType[:], ":")))
	}

	return nil
}

func (p *ParameterCheck) CheckIfSpecialCases(attrAndType []string) error {
	switch attrAndType[1] {
	case "float32", "float64", "float":
		if len(attrAndType) < 5 {
			return errors.New(fmt.Sprintf("Wrong syntax: %s\nYou have to pass a fifth value when using float, float32 or float64", strings.Join(attrAndType[:], ":")))
		}
	}
	return nil
}

func (p *ParameterCheck) CheckComponents(argument string, attrAndType []string) error {
	for i, piece := range attrAndType {
		if len(piece) <= 0 {
			message := ""
			switch i {
			case 0:
				message = "Attribute name is missing."
			case 1:
				message = "Attribute type is missing."
			case 2:
				message = "Start position is missing."
			case 3:
				message = "End position is missing."
			}

			return errors.New(fmt.Sprintf("\nWrong syntax: %s \n%s", argument, message))
		}
	}
	return nil
}
