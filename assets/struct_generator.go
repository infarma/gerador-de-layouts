package assets

import (
	"fmt"
	"github.com/iancoleman/strcase"
	"os"
)

type StructGenerator struct {
	StructFile     *os.File
	PositionsFile  *os.File
	Parameter      Parameter
	ParameterCheck ParameterCheck
	PackageClass   PackageClass
}

func (s *StructGenerator) GenerateClass(arguments []string) error {
	/* Check if there are syntax errors in the parameter list passed */
	err := s.ParameterCheck.CheckParams(arguments)
	if err != nil {
		return err
	}

	/* Compose Layout File */
	s.Parameter.ComposeParameters(arguments)

	/* Create folder and files */
	err = s.composeFolderAndFiles()
	if err != nil {
		return err
	}

	/* Generate layout code */
	s.Parameter.GenerateLayout()

	/* Write Layout code to file */
	err = s.writeFiles()
	if err != nil {
		return err
	}

	fmt.Printf("Layout %s successfully generated.\n", s.Parameter.StructName)

	return err
}

func (s *StructGenerator) writeFiles() error {
	return s.writeStruct()
}

func (s *StructGenerator) writeStruct() error {
	_, err := s.StructFile.WriteString(s.Parameter.StructContent.String())
	if err != nil {
		return err
	}

	defer s.StructFile.Close()
	return nil
}

func (s *StructGenerator) composeFolderAndFiles() error {
	/* Create package folder if it doesn't exist */
	if _, err := os.Stat(s.Parameter.returnPackageFolder()); os.IsNotExist(err) {
		err := os.Mkdir(s.Parameter.returnPackageFolder(), os.ModePerm)
		if err != nil {
			return err
		}
	}

	/* Create file to store struct */
	err := s.createStructFile()
	if err != nil {
		return err
	}

	return nil
}

func (s *StructGenerator) createStructFile() error {
	structPath := s.Parameter.returnPackageFolder()
	f, err := os.Create(fmt.Sprintf("%s//%s.go", structPath, strcase.ToLowerCamel(s.Parameter.StructName)))
	if err != nil {
		return err
	}

	s.StructFile = f
	return nil
}