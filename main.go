package main

import (
	"fmt"
	"gerador-de-layouts/assets"

	"os"
)

func main() {
	var structGenerator assets.StructGenerator
	err := structGenerator.GenerateClass(os.Args)

	if err != nil {
		fmt.Println("Error while creating layout. ", err.Error())
		os.Exit(0)
	}
}
